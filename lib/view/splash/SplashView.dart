import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:newsportal/base/BaseView.dart';
import 'package:newsportal/colors/colors.dart';
import 'package:newsportal/response/ResponseNews.dart';
import 'package:newsportal/string/Strings.dart';
import 'package:newsportal/view/newsdetail/NewsDetailView.dart';
import 'SplashPresenter.dart';
import 'SplashViewCallback.dart';

class SplashView extends BaseView<SplashState> {
  @override
  SplashState state() => SplashState();
}

class SplashState extends BaseState<SplashPresenter, SplashView> implements SplashViewCallback {
  final GlobalKey<ScaffoldState> scaffold_key = new GlobalKey<ScaffoldState>();
  bool status_internet = false;
  ResponseNews responseNews;
  bool status_responsenews=false;


  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    presenter.dispose();
    super.dispose();
  }

  @override
  Widget create(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: CustomColor.themecolor));
    return Scaffold(
      key: scaffold_key,
      appBar: AppBar(
        title: Text(
          "News",
          style: TextStyle(color: CustomColor.themecolor),
        ),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: <Widget>[
          FlatButton(
              onPressed: () {
              },
              child: Icon(
                Icons.search,
                color: CustomColor.themecolor,
              )),
        ],
      ),
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                height: 300,
                child: status_responsenews?ListView.builder(
                  physics: const AlwaysScrollableScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: responseNews.articles.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
                      child:FlatButton(
                        onPressed: (){
                          push(NewsDetailView(url: responseNews.articles[index].url,),withReplacement: false);
                        },
                        child:Container(
                          margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child:ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child:FadeInImage.assetNetwork(
                              placeholder:"assets/images/placeholder.png",
                              image:responseNews.articles[index].urlToImage!="null"?responseNews.articles[index].urlToImage:"",
                              fit: BoxFit.cover,
                              height: 300,
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ):Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                  ],
                ),
              ),

              status_responsenews?ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: responseNews.articles.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
                    child:FlatButton(
                      onPressed: (){
                        push(NewsDetailView(url: responseNews.articles[index].url,),withReplacement: false);
                      },
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            flex: 30,
                            child:  Container(
                              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              child:ClipRRect(
                                borderRadius: BorderRadius.circular(5),
                                child:FadeInImage.assetNetwork(
                                  placeholder:"assets/images/placeholder.png",
                                  image:responseNews.articles[index].urlToImage!="null"?responseNews.articles[index].urlToImage:"",
                                  fit: BoxFit.cover,
                                  width: 100,
                                  height: 100,
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 70,
                            child:Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child:Text(
                                    responseNews.articles[index].title,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color:Colors.black,
                                      letterSpacing: 0,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Sfdbold',
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child:Text(
                                    responseNews.articles[index].description,
                                    textAlign: TextAlign.left,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      color:Colors.black,
                                      letterSpacing: 0,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'Avenir',
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ):Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                ],
              ),
            ],
          )
        ),
    );
  }


  @override
  onCheckInternetComplete(bool isConnection) {
    setState(() {
      if (isConnection) {
        presenter.doNewsApiCall("us");
      }
      else
      {
        scaffold_key.currentState.showSnackBar(SnackBar(
          content: Text(Strings.nointernetconnection),
          duration: Duration(hours: 5),
          action: SnackBarAction(
            label: 'RETRY',
            textColor: CustomColor.themecolor,
            onPressed: () => presenter.checkConnection(),
          ),
        ));
      }
    });
  }

  @override
  onNewsLoaded(ResponseNews responseNews) {
    setState(() {
      status_responsenews=true;
      this.responseNews=responseNews;
    });
  }
}
