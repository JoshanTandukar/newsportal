

import 'package:newsportal/base/BaseViewCallback.dart';
import 'package:newsportal/response/ResponseNews.dart';

abstract class SplashViewCallback extends BaseViewCallback {
  onCheckInternetComplete(bool isConnection);
  onNewsLoaded(ResponseNews responseNews);
}
