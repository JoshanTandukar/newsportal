

import 'package:newsportal/base/BasePresenter.dart';
import 'package:newsportal/domain/repository/AuthRepository.dart';
import 'package:newsportal/domain/repository/ContentRepository.dart';
import 'package:newsportal/domain/repository/NetworkRepository.dart';
import 'package:newsportal/domain/repository/SecureStorageRepository.dart';

import 'SplashView.dart';

class SplashPresenter extends BasePresenter<SplashState> {
  NetworkRepository networkeRepo;
  AuthRepository authRepo;
  SecureStorageRepository securestorageRepo;
  ContentRepository contentRepo;
  SplashPresenter(this.networkeRepo,this.securestorageRepo,this.authRepo,this.contentRepo);

  @override
  init() {
    super.init();
    listenToConnectivityChanges(true);
    checkConnection();
  }

  @override
  dispose() {
    super.dispose();
  }

  checkConnection() async {
    networkeRepo
        .checkConnection()
        .then((connectivityResult) =>
        view.onCheckInternetComplete(isConnection(connectivityResult)))
        .catchError(view.onError);
  }

  Future<void> doNewsApiCall(String country) async {
    view.showProgress();
    contentRepo.doNewsApiCall(country).then((posts) {
      view.onNewsLoaded(posts);
      view.hideProgress();
    }).catchError((error) {
      view.onError(error);
      view.hideProgress();
    });
  }
}
