import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:newsportal/base/BaseView.dart';
import 'package:newsportal/colors/colors.dart';
import 'package:newsportal/string/Strings.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'NewsDetailPresenter.dart';
import 'package:html/dom.dart' as dom;

import 'NewsDetailViewCallback.dart';

class NewsDetailView extends BaseView<NewsDetailState> {

  String url;
  NewsDetailView({Key key,this.url});

  @override
  NewsDetailState state() => NewsDetailState();
}

class NewsDetailState extends BaseState<NewsDetailPresenter, NewsDetailView> implements NewsDetailViewCallback {
  final GlobalKey<ScaffoldState> scaffold_key = new GlobalKey<ScaffoldState>();
  bool status_internet = false;
  String resposne="";
  bool status_response=false;
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    presenter.dispose();
    super.dispose();
  }

  @override
  Widget create(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: CustomColor.themecolor));
    return WebviewScaffold(
      url: widget.url,
    );
  }

  @override
  onCheckInternetComplete(bool isConnection) {
    setState(() {
      if (isConnection) {
      }
      else
      {
        scaffold_key.currentState.showSnackBar(SnackBar(
          content: Text(Strings.nointernetconnection),
          duration: Duration(hours: 5),
          action: SnackBarAction(
            label: 'RETRY',
            textColor: CustomColor.themecolor,
            onPressed: () => presenter.checkConnection(),
          ),
        ));
      }
    });
  }

  @override
  onNewsDetailLoaded(String response) {
    setState(() {
      status_response=true;
      this.resposne=response;
    });
  }
}
