import 'package:newsportal/base/BasePresenter.dart';
import 'package:newsportal/domain/repository/AuthRepository.dart';
import 'package:newsportal/domain/repository/ContentRepository.dart';
import 'package:newsportal/domain/repository/NetworkRepository.dart';
import 'package:newsportal/domain/repository/SecureStorageRepository.dart';

import 'NewsDetailViewCallback.dart';

class NewsDetailPresenter extends BasePresenter<NewsDetailViewCallback> {
  ContentRepository contentRepo;
  SecureStorageRepository secureRepo;
  NetworkRepository networkRepo;
  AuthRepository authRepo;

  NewsDetailPresenter(this.contentRepo, this.secureRepo, this.networkRepo, this.authRepo);
  @override
  init() {
    super.init();
    listenToConnectivityChanges(true);
    checkConnection();
  }

  checkConnection() async {
    networkRepo
        .checkConnection()
        .then((connectivityResult) =>
        view.onCheckInternetComplete(isConnection(connectivityResult)))
        .catchError(view.onError);
  }
}
