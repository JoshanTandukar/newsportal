

import 'package:newsportal/base/BaseViewCallback.dart';

abstract class NewsDetailViewCallback extends BaseViewCallback {
  onCheckInternetComplete(bool isConnection);
  onNewsDetailLoaded(String response);
}
