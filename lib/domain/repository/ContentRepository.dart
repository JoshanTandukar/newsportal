

import 'package:newsportal/response/ResponseNews.dart';


abstract class ContentRepository {

  Future<ResponseNews> doNewsApiCall(String country);
  Future<String> doNewsDetailApiCall(String url);
}
