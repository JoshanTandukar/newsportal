import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class SecureStorageRepository {


  //Login Details
  Future<void> saveApiToken(String value);
  Future<String> getApiToken();
  Future<void> deleteApiToken();

  Future<void> saveLoginId(String value);
  Future<String> getLoginId();
  Future<void> deleteLoginId();

  Future<void> saveLoginName(String value);
  Future<String> getLoginName();
  Future<void> deleteLoginName();

  Future<void> saveLoginEmail(String value);
  Future<String> getLoginEmail();
  Future<void> deleteLoginEmail();

  Future<void> saveLoginMobileno(String value);
  Future<String> getLoginMobileno();
  Future<void> deleteLoginMobileno();

  Future<void> saveLoginAddress(String value);
  Future<String> getLoginAddress();
  Future<void> deleteLoginAddress();

  Future<void> saveIsActive(String value);
  Future<String> getIsActive();
  Future<void> deleteIsActive();

  Future<void> saveLatitude(String value);
  Future<String> getLatitude();
  Future<void> deleteLatitude();

  Future<void> saveLongitude(String value);
  Future<String> getLongitude();
  Future<void> deleteLongitude();

  Future<void> saveLoginImage(String value);
  Future<String> getLoginImage();
  Future<void> deleteLoginImage();

  //Login Close

  Future<void> saveNotificationCount(String value);
  Future<String> getNotificationCount();
  Future<void> deleteNotificationCount();
}
