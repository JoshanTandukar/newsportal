
import 'package:flutter/material.dart';
import 'package:newsportal/string/Strings.dart';
import 'package:newsportal/view/splash/SplashView.dart';

import 'AppInjector.dart';

main() {
  AppInjector.inject();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Strings.appName,
      home: SplashView(), color: Colors.red.shade500,
    );
  }
}
