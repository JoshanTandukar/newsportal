

import 'package:newsportal/domain/entity/Entity.dart';

class ResponseNews extends Entity {
  String status;
  String totalResults;
  List<Articles> articles;

  ResponseNews.fromJson(Map<String, dynamic> json) {
    status = (json['status']).toString();
    totalResults = (json['totalResults']).toString();
    if (json['articles'] != null) {
      articles = new List<Articles>();
      json['articles'].forEach((v) {
        articles.add(new Articles.fromJson(v));
      });
    }
  }
}

class Articles {
  Source source;
  String author;
  String title;
  String description;
  String url;
  String urlToImage;
  String publishedAt;
  String content;

  Articles.fromJson(Map<String, dynamic> json) {
    source = json['source'] != null ? new Source.fromJson(json['source']) : null;
    author = (json['author']).toString();
    title = (json['title']).toString();
    description = (json['description']).toString();
    url = (json['url']).toString();
    urlToImage = (json['urlToImage']).toString();
    publishedAt = (json['publishedAt']).toString();
    content = (json['content']).toString();
  }
}

class Source {
  String id;
  String name;

  Source.fromJson(Map<String, dynamic> json) {
    id = (json['id']).toString();
    name = (json['name']).toString();
  }
}