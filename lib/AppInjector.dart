
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:newsportal/repository/AuthRepositoryImpl.dart';
import 'package:newsportal/repository/NetworkRepositoryImpl.dart';
import 'package:newsportal/repository/SecureStorageRepositoryImpl.dart';
import 'package:newsportal/view/newsdetail/NewsDetailPresenter.dart';
import 'package:newsportal/view/splash/SplashPresenter.dart';
import 'domain/repository/AuthRepository.dart';
import 'domain/repository/ContentRepository.dart';
import 'domain/repository/NetworkRepository.dart';
import 'domain/repository/SecureStorageRepository.dart';
import 'repository/ContentRepositoryImpl.dart';

class AppInjector {
  static const String BASE_URL = "base_url";

  static inject() {
    final injector = Injector.getInjector();

    //strings
    injector.map<String>((i) => "http://newsapi.org/v2/top-headlines?", key: BASE_URL);

    //repositories
    injector.map<NetworkRepository>((i) => NetworkRepositoryImpl(), isSingleton: true);
    injector.map<SecureStorageRepository>((i) => SecureStorageRepositoryImpl(), isSingleton: true);
    injector.map<AuthRepository>((i) => AuthRepositoryImpl(i.get(key: BASE_URL),i.get()), isSingleton: true);
    injector.map<ContentRepository>((i) => ContentRepositoryImpl(i.get(key: BASE_URL),i.get(),i.get()), isSingleton: true);


    //presenters
    injector.map<SplashPresenter>((i) => SplashPresenter(i.get(),i.get(),i.get(),i.get()));
    injector.map<NewsDetailPresenter>((i) => NewsDetailPresenter(i.get(),i.get(),i.get(),i.get()));

  }
}
