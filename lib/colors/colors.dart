

import 'dart:ui';

class CustomColor {
  static dynamic themecolor = const Color(0xFFDC691D);
  static dynamic cyan = const Color(0xFF008490);
  static dynamic textcolor_grey = const Color(0xFFD2D2D2);
  static dynamic textcolor_greydark = const Color(0xFF9C9C9C);
  static dynamic themecolor_transparent = const Color(0x50DC691D);
  static dynamic black_transparent = const Color(0x30000000);
  static dynamic red_transparent = const Color(0x30000000);
  static dynamic white_transparent = const Color(0x30ffffff);
}
