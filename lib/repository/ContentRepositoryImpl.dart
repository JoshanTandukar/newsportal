import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:newsportal/domain/repository/AuthRepository.dart';
import 'package:newsportal/domain/repository/ContentRepository.dart';
import 'package:newsportal/domain/repository/SecureStorageRepository.dart';
import 'package:newsportal/response/ResponseNews.dart';
import 'package:newsportal/string/Strings.dart';

class ContentRepositoryImpl extends ContentRepository {
  String base_url;
  String _results = 'data';
  SecureStorageRepository secureRepo;
  AuthRepository authRepo;
  ContentRepositoryImpl(this.base_url, this.secureRepo, this.authRepo);

  Future<Map<String, dynamic>> _request<D>(String endpoint) async {
    final uri = Uri.https(base_url, endpoint, await getApiToken());
    Response response = await http.get(uri);
    return jsonDecode(response.body);
  }

  Future<Map<String, String>> getApiToken() async {
    final accessToken = await secureRepo.getApiToken();
    return {Strings.accessToken: accessToken};
  }

  @override
  Future<ResponseNews> doNewsApiCall(String country) async {
    String newsurl = base_url+"country="+country+"&apiKey=cdb3e222b166493a9f76c9940a3c2e57";
    http.Client client=new http.Client();
    final response = await client.get(newsurl);
    print("url ${newsurl} reponse ${response.body.toString()}");
    if (response.statusCode == 200)
    {
      return ResponseNews.fromJson(json.decode(response.body));
    }
    else if (response.statusCode == 401)
    {
      return ResponseNews.fromJson(json.decode(response.body));
    }
    else if (response.statusCode == 422)
    {
      return ResponseNews.fromJson(json.decode(response.body));
    }
    else
    {
      throw Exception(response.body);
    }
  }

  @override
  Future<String> doNewsDetailApiCall(String url) async {
    http.Client client=new http.Client();
    final response = await client.get(url);
    print("url ${url} reponse ${response.body.toString()}");
    if (response.statusCode == 200)
    {
      return response.body.toString();
    }
    else if (response.statusCode == 401)
    {
      return response.body.toString();
    }
    else if (response.statusCode == 422)
    {
      return response.body.toString();
    }
    else
    {
      throw Exception(response.body);
    }
  }
}
