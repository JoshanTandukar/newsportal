
import 'package:connectivity/connectivity.dart';
import 'package:newsportal/domain/repository/NetworkRepository.dart';

class NetworkRepositoryImpl extends NetworkRepository {
  @override
  Future<ConnectivityResult> checkConnection() async {
    return Connectivity().checkConnectivity();
  }
}
