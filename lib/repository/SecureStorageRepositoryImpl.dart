
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:newsportal/domain/repository/SecureStorageRepository.dart';

class SecureStorageRepositoryImpl extends SecureStorageRepository {

  //Login Pref
  static const String STORAGE_KEY_TOKEN = "api_token";
  static const String STORAGE_KEY_LOGINID = "login_id";
  static const String STORAGE_KEY_NAME = "login_name";
  static const String STORAGE_KEY_EMAIL = "login_email";
  static const String STORAGE_KEY_MOBILENO = "login_mobileno";
  static const String STORAGE_KEY_ADDRESS = "login_address";
  static const String STORAGE_KEY_ISACTIVE = "login_isactive";
  static const String STORAGE_KEY_LATITUDE =  "latitude";
  static const String STORAGE_KEY_LONGITUDE =  "longitude";
  static const String STORAGE_KEY_LOGINIMAGE =  "login_image";
  static const String STORAGE_KEY_NOTIFICATIONCOUNT =  "0";



  final storage = FlutterSecureStorage();

  @override
  Future<void> saveApiToken(String value) async {
    return _saveApiToken(STORAGE_KEY_TOKEN, value);
  }

  Future<void> _saveApiToken(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getApiToken() {
    return _getApiToken(STORAGE_KEY_TOKEN);
  }

  Future<String> _getApiToken(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteApiToken() {
    return _deleteApiToken(STORAGE_KEY_TOKEN);
  }

  Future<void> _deleteApiToken(String key) async {
    return storage.delete(key: key);
  }

  @override
  Future<void> saveLoginId(String value) async {
    return _saveLoginId(STORAGE_KEY_LOGINID, value);
  }

  Future<void> _saveLoginId(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getLoginId() {
    return _getLoginId(STORAGE_KEY_LOGINID);
  }

  Future<String> _getLoginId(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteLoginId() {
    return _deleteLoginId(STORAGE_KEY_LOGINID);
  }

  Future<void> _deleteLoginId(String key) async {
    return storage.delete(key: key);
  }

  @override
  Future<void> saveLoginName(String value) async {
    return _saveLoginName(STORAGE_KEY_NAME, value);
  }

  Future<void> _saveLoginName(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getLoginName() {
    return _getLoginName(STORAGE_KEY_NAME);
  }

  Future<String> _getLoginName(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteLoginName() {
    return _deleteLoginName(STORAGE_KEY_NAME);
  }

  Future<void> _deleteLoginName(String key) async {
    return storage.delete(key: key);
  }

  @override
  Future<void> saveLoginEmail(String value) async {
    return _saveLoginEmail(STORAGE_KEY_EMAIL, value);
  }

  Future<void> _saveLoginEmail(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getLoginEmail() {
    return _getLoginEmail(STORAGE_KEY_EMAIL);
  }

  Future<String> _getLoginEmail(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteLoginEmail() {
    return _deleteLoginEmail(STORAGE_KEY_EMAIL);
  }

  Future<void> _deleteLoginEmail(String key) async {
    return storage.delete(key: key);
  }

  @override
  Future<void> saveLoginMobileno(String value) async {
    return _saveLoginMobileno(STORAGE_KEY_MOBILENO, value);
  }

  Future<void> _saveLoginMobileno(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getLoginMobileno() {
    return _getLoginMobileno(STORAGE_KEY_MOBILENO);
  }

  Future<String> _getLoginMobileno(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteLoginMobileno() {
    return _deleteLoginMobileno(STORAGE_KEY_MOBILENO);
  }

  Future<void> _deleteLoginMobileno(String key) async {
    return storage.delete(key: key);
  }

  @override
  Future<void> saveLoginAddress(String value) async {
    return _saveLoginAddress(STORAGE_KEY_ADDRESS, value);
  }

  Future<void> _saveLoginAddress(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getLoginAddress() {
    return _getLoginAddress(STORAGE_KEY_ADDRESS);
  }

  Future<String> _getLoginAddress(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteLoginAddress() {
    return _deleteLoginAddress(STORAGE_KEY_ADDRESS);
  }

  Future<void> _deleteLoginAddress(String key) async {
    return storage.delete(key: key);
  }

  @override
  Future<void> saveLatitude(String value) async {
    return _saveLatitude(STORAGE_KEY_LATITUDE,value);
  }

  Future<void> _saveLatitude(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getLatitude() async{
    return _getLatitude(STORAGE_KEY_LATITUDE);
  }

  Future<String> _getLatitude(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteLatitude() {
    return _deleteLatitude(STORAGE_KEY_LATITUDE);
  }

  Future<void> _deleteLatitude(String key) async {
    return storage.delete(key: key);
  }

  @override
  Future<void> saveLongitude(String value) async {
    return _saveLongitude(STORAGE_KEY_LONGITUDE, value);
  }

  Future<void> _saveLongitude(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getLongitude() async{
    return _getLongitude(STORAGE_KEY_LONGITUDE);
  }

  Future<String> _getLongitude(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteLongitude() {
    return _deleteLongitude(STORAGE_KEY_LONGITUDE);
  }

  Future<void> _deleteLongitude(String key) async {
    return storage.delete(key: key);
  }

  Future<void> saveIsActive(String value) async {
    return _saveIsActive(STORAGE_KEY_ISACTIVE, value);
  }

  Future<void> _saveIsActive(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getIsActive() {
    return _getIsActive(STORAGE_KEY_ISACTIVE);
  }

  Future<String> _getIsActive(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteIsActive() {
    return _deleteIsActive(STORAGE_KEY_ISACTIVE);
  }

  Future<void> _deleteIsActive(String key) async {
    return storage.delete(key: key);
  }

  Future<void> saveLoginImage(String value) async {
    return _saveLoginImage(STORAGE_KEY_LOGINIMAGE, value);
  }

  Future<void> _saveLoginImage(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getLoginImage() {
    return _getLoginImage(STORAGE_KEY_LOGINIMAGE);
  }

  Future<String> _getLoginImage(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteLoginImage() {
    return _deleteLoginImage(STORAGE_KEY_LOGINIMAGE);
  }

  Future<void> _deleteLoginImage(String key) async {
    return storage.delete(key: key);
  }

  Future<void> saveNotificationCount(String value) async {
    return _saveNotificationCount(STORAGE_KEY_NOTIFICATIONCOUNT, value);
  }

  Future<void> _saveNotificationCount(String key, String value) async {
    return storage.write(key: key, value: value);
  }

  @override
  Future<String> getNotificationCount() {
    return _getNotificationCount(STORAGE_KEY_NOTIFICATIONCOUNT);
  }

  Future<String> _getNotificationCount(String key) async {
    return storage.read(key: key);
  }

  @override
  Future<void> deleteNotificationCount() {
    return _deleteNotificationCount(STORAGE_KEY_NOTIFICATIONCOUNT);
  }

  Future<void> _deleteNotificationCount(String key) async {
    return storage.delete(key: key);
  }

}
