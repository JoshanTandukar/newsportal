import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:newsportal/domain/repository/AuthRepository.dart';
import 'package:newsportal/domain/repository/SecureStorageRepository.dart';

class AuthRepositoryImpl extends AuthRepository {

  String base_url;
  String _results = 'data';
  SecureStorageRepository secureRepo;
  AuthRepository authRepo;

  AuthRepositoryImpl(this.base_url, this.secureRepo);

  @override
  Future<bool> authorize() async {
    String banner=base_url+'myprofile';

    var map = new Map<String, dynamic>();
    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/x-www-form-urlencoded',
      'Authorization': await secureRepo.getApiToken(),
    };
    final response = await http.post(
        '$banner?$_results=${toString()}',
        headers: requestHeaders);
    if (response.statusCode == 200)
    {
      return true;
    }
    else if(response.statusCode ==401)
    {
      logout();
      return false;
    }
    else{
      return true;
    }
  }

  @override
  Future<bool> logout() async {
    await secureRepo.deleteApiToken();
    await secureRepo.deleteLoginId();
    await secureRepo.deleteLoginName();
    await secureRepo.deleteLoginEmail();
    await secureRepo.deleteLoginMobileno();
    await secureRepo.deleteLoginAddress();
    await secureRepo.deleteLatitude();
    await secureRepo.deleteLongitude();
    return true;
  }

  @override
  Future<bool> isLoggedIn() async {
    final token = await secureRepo.getApiToken();
    return Future.value(token != null);
  }
}
